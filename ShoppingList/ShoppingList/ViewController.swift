//
//  ViewController.swift
//  ShoppingList
//
//  Created by gio emiliano on 2019-10-16.
// Student ID: 301044051

//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    let cellReuseIdentifier = "ItemCell"
    
    var shoppingList = ShoppingList()
    
    var editingItem: Item = Item()


    @IBOutlet weak var addUpdateButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var tableViewItens: UITableView!
    
    @IBOutlet weak var listName: UITextField!
    
    @IBOutlet weak var newItemName: UITextField!
    @IBOutlet weak var newItemQtd: UILabel!
    
    var qtd = 1
    
    var isUpdating = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableViewItens.delegate = self
        tableViewItens.dataSource = self
        
        tableViewItens.allowsMultipleSelectionDuringEditing = false;

        
        resetFields()
    }
    
    
    func resetFields(){
        qtd = 1
        newItemName.text = ""
        newItemQtd.text = String(qtd)
        addUpdateButton.setTitle("Add", for: .normal)
        isUpdating = false
        deleteButton.isHidden = true
        cancelButton.isHidden = true


        
    }
    
    func update(item: Item){
        self.editingItem = item
        deleteButton.isHidden = false
        cancelButton.isHidden = false

        addUpdateButton.setTitle("Update", for: .normal)

        qtd = self.editingItem.quantity
        newItemName.text = self.editingItem.title
        newItemQtd.text = String(qtd)
        isUpdating = true

    }
    @IBAction func cancelNewItem(_ sender: Any) {
        
        resetFields()
        tableViewItens.reloadData()
    }
    
    @IBAction func cancel(_ sender: Any) {
    }
    @IBAction func deleteItem(_ sender: Any) {
        
        shoppingList.listOfItens.remove(at: self.editingItem.id)
        rearrengeList()
        tableViewItens.reloadData()
        resetFields()

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shoppingList.listOfItens.count
    }
    
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ItemCell
        
        let item = shoppingList.listOfItens[indexPath.row]
        
        cell.setup(withItem: item)
        
        // set the text from the data model
        //cell.textLabel?.text = self.animals[indexPath.row]
        
        return cell
    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        let item = shoppingList.listOfItens[indexPath.row]

        update(item: item)
    }
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
  
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            //add code here for when you hit delete
            
            // remove the item from the data model
            shoppingList.listOfItens.remove(at: indexPath.row)
            rearrengeList()
            
            // delete the table view row
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
 
    @IBAction func decreseQtd(_ sender: Any) {
        
        if (qtd == 1) { return }
        qtd = qtd - 1
        newItemQtd.text = String(qtd)
    }
    
    @IBAction func increaseQtd(_ sender: Any) {
        
        qtd = qtd + 1
        newItemQtd.text = String(qtd)
    }
    
    @IBAction func addNewItem(_ sender: Any) {
        
        guard let title = newItemName.text else { return }
        
        if (title == "")  {
            simpleAlertWith(message: "Item needs a name!", andTitle: "Error")
            return
        }
        
        
        if (isUpdating == true) {
            let newItem = Item(title: title, quantity: qtd,id:editingItem.id)

            shoppingList.listOfItens[self.editingItem.id] = newItem
            
           
        } else {
            let newItem = Item(title: title, quantity: qtd,id:shoppingList.listOfItens.count)

            shoppingList.listOfItens.append(newItem)

        }
        
        tableViewItens.reloadData()
        resetFields()

        
    }
    
    
    func rearrengeList(){
        var index = 0
        for item in shoppingList.listOfItens {
            item.id = index
            index = index + 1
        }
    }

    @IBAction func save(_ sender: Any) {
        
        guard let listName = listName.text else { return }
        
        if (listName == "")  {
            simpleAlertWith(message: "List needs a name!", andTitle: "Error")
            return
        }
        
        
        self.shoppingList.name = listName
        if ( DataBase().saveShoppingList(shoppingList: self.shoppingList) == true) {
            simpleAlertWith(message: "List has been saved", andTitle: "Saved")
        }
    }
    
   
    
}


extension ViewController {
    
    func simpleAlertWith(message : String, andTitle: String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
}




