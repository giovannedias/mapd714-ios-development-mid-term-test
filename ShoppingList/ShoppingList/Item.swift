//
//  Item.swift
//  ShoppingList
//
// Student ID: 301044051

//  Created by gio emiliano on 2019-10-16.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation


class Item  {
    var title = ""
    var quantity = 0
    var id = -1
    
    init(title:String, quantity: Int, id: Int) {
        self.title = title
        self.quantity = quantity
        self.id = id
    }
    
    init(){
        
    }
    
    func toDictionary()->NSDictionary {
        
        let dictionary: NSDictionary = [
            "title" : title,
            "quantity" : quantity,
            "id" : id
        ]
        
        print(dictionary)
 
        return dictionary
    }
    
  
}
