//
//  ShoppingList.swift
//  ShoppingList
//
// Student ID: 301044051

//  Created by gio emiliano on 2019-10-16.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation


class ShoppingList {
    var name : String  = ""
    var listOfItens : [Item] = []
    
    
    init(){
        
    }
    init(name : String, listOfItens: [Item]) {
        self.name = name
        self.listOfItens = listOfItens
    }
    
    
    func toDictionary()->NSDictionary {
        
        var arrayOfDictionaryItens = Array<Any>()
        
        for item in listOfItens {
        
            let dictionary = item.toDictionary()
            print(dictionary)
            arrayOfDictionaryItens.append(dictionary)
        }
        
        
        let dictionary: NSDictionary = [
            "name" : name,
            "listOfItens" : arrayOfDictionaryItens
        ]
        
        print(dictionary)
  
        
        return dictionary
    }
    
    
   
}
