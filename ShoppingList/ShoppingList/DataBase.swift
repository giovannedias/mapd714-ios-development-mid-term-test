//
//  DataBase.swift
//  ShoppingList
//
// Student ID: 301044051

//  Created by gio emiliano on 2019-10-16.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation


class  DataBase  {
    
    
    func saveShoppingList(shoppingList : ShoppingList) -> Bool {
        
        let shoppingListdictionary = shoppingList.toDictionary()

        
        if var dbsavedList = UserDefaults.standard.array(forKey: "SavedShoopingList") as? Array<Any>{
            
            print(dbsavedList)
            
            dbsavedList.append(shoppingListdictionary)
            
            UserDefaults.standard.set(dbsavedList, forKey: "SavedShoopingList")

            return true//setObject

        }
        
        
        var savedList = Array<Any>()
        
        savedList.append(shoppingListdictionary)
        
        print(savedList)
        
        UserDefaults.standard.set(savedList, forKey: "SavedShoopingList")
        
        return true
    }
    
    func getSavedList()->[ShoppingList] {
        
        var listOfShoppingLists : [ShoppingList] = []

        if var dbsavedList = UserDefaults.standard.array(forKey: "SavedShoopingList") as? Array<Any>{
            
            
            for anyShoppingList in dbsavedList {
                let shoppingList = ShoppingList()
                
                if let dictionaryShoppingList  = anyShoppingList as? NSDictionary {

                    if let name = dictionaryShoppingList["name"] as? String {
                        shoppingList.name = name
                    }
                    
                    if let arrayListItens  = dictionaryShoppingList["listOfItens"] as? NSArray {
                        for anyItem in arrayListItens {
                            if let dicItem = anyItem as? NSDictionary {
                                
                                let item = Item()
                                if let title = dicItem["title"] as? String {
                                    item.title = title
                                    
                                }
                                if let id = dicItem["id"] as? Int {
                                    item.id = id
                                    
                                    
                                    
                                }
                                if let qtd = dicItem["quantity"] as? Int {
                                    
                                        item.quantity = qtd
                                    
                                }
                                shoppingList.listOfItens.append(item)

                            }

                        }
                        
                    }
                    
                    listOfShoppingLists.append(shoppingList)
                    
                    
                }

                
            }
        }
            

                

            
            
            return listOfShoppingLists
        
    }

}
