//
//  MenuViewControlle.swift
//  ShoppingList
//
// Student ID: 301044051

//  Created by gio emiliano on 2019-10-16.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import UIKit

class MenuViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    let cellReuseIdentifier = "ListCell"

    @IBOutlet weak var tableViewList: UITableView!
    
    var listOfShoppingLists : [ShoppingList] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableViewList.delegate = self
        tableViewList.dataSource = self
        
        
        
       

        
        //tableViewItens.allowsMultipleSelectionDuringEditing = false;
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.listOfShoppingLists = DataBase().getSavedList()
        tableViewList.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfShoppingLists.count
    }
    
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UITableViewCell
        
        let list = listOfShoppingLists[indexPath.row]
        
        cell.textLabel?.text = list.name
        
        return cell
    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        let list = listOfShoppingLists[indexPath.row]
        //SegueList
        
        performSegue(withIdentifier: "SegueList", sender: list)


        

    

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is ViewController
        {
            let vc = segue.destination as? ViewController
            if let list = sender as? ShoppingList {
                vc?.shoppingList = list
            }
        }
    }
    
    /*
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            //add code here for when you hit delete
            
            // remove the item from the data model
            shoppingList.listOfItens.remove(at: indexPath.row)
            rearrengeList()
            
            // delete the table view row
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }*/
}
