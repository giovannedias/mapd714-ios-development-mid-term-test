//
//  ItemCell.swift
//  ShoppingList
//
// Student ID: 301044051

//  Created by gio emiliano on 2019-10-16.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import UIKit

class ItemCell : UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var qtd: UILabel!
    
    var item: Item = Item()
    
    func setup(withItem item: Item) {
        
        self.item = item
        self.title.text = self.item.title
        self.qtd.text = String(self.item.quantity)
        
        
    }
    
    
}
